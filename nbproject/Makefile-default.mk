#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Kernel/alarm.c Kernel/pro_man.c Kernel/scheduler.s Kernel/even_man.c Kernel/int_man.c Project/main.c Project/lin.c Project/taskdesc.c Project/timer.c Project/uart.c Project/cbuf.c Project/can.c Project/stalk.c Project/tsk_stalk.c Project/flight.c Project/tsk_flightnot.c Project/tsk_flightctrl.c Project/tsk_idle.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Kernel/alarm.o ${OBJECTDIR}/Kernel/pro_man.o ${OBJECTDIR}/Kernel/scheduler.o ${OBJECTDIR}/Kernel/even_man.o ${OBJECTDIR}/Kernel/int_man.o ${OBJECTDIR}/Project/main.o ${OBJECTDIR}/Project/lin.o ${OBJECTDIR}/Project/taskdesc.o ${OBJECTDIR}/Project/timer.o ${OBJECTDIR}/Project/uart.o ${OBJECTDIR}/Project/cbuf.o ${OBJECTDIR}/Project/can.o ${OBJECTDIR}/Project/stalk.o ${OBJECTDIR}/Project/tsk_stalk.o ${OBJECTDIR}/Project/flight.o ${OBJECTDIR}/Project/tsk_flightnot.o ${OBJECTDIR}/Project/tsk_flightctrl.o ${OBJECTDIR}/Project/tsk_idle.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Kernel/alarm.o.d ${OBJECTDIR}/Kernel/pro_man.o.d ${OBJECTDIR}/Kernel/scheduler.o.d ${OBJECTDIR}/Kernel/even_man.o.d ${OBJECTDIR}/Kernel/int_man.o.d ${OBJECTDIR}/Project/main.o.d ${OBJECTDIR}/Project/lin.o.d ${OBJECTDIR}/Project/taskdesc.o.d ${OBJECTDIR}/Project/timer.o.d ${OBJECTDIR}/Project/uart.o.d ${OBJECTDIR}/Project/cbuf.o.d ${OBJECTDIR}/Project/can.o.d ${OBJECTDIR}/Project/stalk.o.d ${OBJECTDIR}/Project/tsk_stalk.o.d ${OBJECTDIR}/Project/flight.o.d ${OBJECTDIR}/Project/tsk_flightnot.o.d ${OBJECTDIR}/Project/tsk_flightctrl.o.d ${OBJECTDIR}/Project/tsk_idle.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Kernel/alarm.o ${OBJECTDIR}/Kernel/pro_man.o ${OBJECTDIR}/Kernel/scheduler.o ${OBJECTDIR}/Kernel/even_man.o ${OBJECTDIR}/Kernel/int_man.o ${OBJECTDIR}/Project/main.o ${OBJECTDIR}/Project/lin.o ${OBJECTDIR}/Project/taskdesc.o ${OBJECTDIR}/Project/timer.o ${OBJECTDIR}/Project/uart.o ${OBJECTDIR}/Project/cbuf.o ${OBJECTDIR}/Project/can.o ${OBJECTDIR}/Project/stalk.o ${OBJECTDIR}/Project/tsk_stalk.o ${OBJECTDIR}/Project/flight.o ${OBJECTDIR}/Project/tsk_flightnot.o ${OBJECTDIR}/Project/tsk_flightctrl.o ${OBJECTDIR}/Project/tsk_idle.o

# Source Files
SOURCEFILES=Kernel/alarm.c Kernel/pro_man.c Kernel/scheduler.s Kernel/even_man.c Kernel/int_man.c Project/main.c Project/lin.c Project/taskdesc.c Project/timer.c Project/uart.c Project/cbuf.c Project/can.c Project/stalk.c Project/tsk_stalk.c Project/flight.c Project/tsk_flightnot.c Project/tsk_flightctrl.c Project/tsk_idle.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F4011
MP_LINKER_FILE_OPTION=,--script="Linker\p30F4011.gld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Kernel/alarm.o: Kernel/alarm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/alarm.o.d 
	@${RM} ${OBJECTDIR}/Kernel/alarm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/alarm.c  -o ${OBJECTDIR}/Kernel/alarm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/alarm.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/alarm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/pro_man.o: Kernel/pro_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/pro_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/pro_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/pro_man.c  -o ${OBJECTDIR}/Kernel/pro_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/pro_man.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/pro_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/even_man.o: Kernel/even_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/even_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/even_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/even_man.c  -o ${OBJECTDIR}/Kernel/even_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/even_man.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/even_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/int_man.o: Kernel/int_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/int_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/int_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/int_man.c  -o ${OBJECTDIR}/Kernel/int_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/int_man.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/int_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/main.o: Project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/main.o.d 
	@${RM} ${OBJECTDIR}/Project/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/main.c  -o ${OBJECTDIR}/Project/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/lin.o: Project/lin.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/lin.o.d 
	@${RM} ${OBJECTDIR}/Project/lin.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/lin.c  -o ${OBJECTDIR}/Project/lin.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/lin.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/lin.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/taskdesc.o: Project/taskdesc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/taskdesc.o.d 
	@${RM} ${OBJECTDIR}/Project/taskdesc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/taskdesc.c  -o ${OBJECTDIR}/Project/taskdesc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/taskdesc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/taskdesc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/timer.o: Project/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/timer.o.d 
	@${RM} ${OBJECTDIR}/Project/timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/timer.c  -o ${OBJECTDIR}/Project/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/uart.o: Project/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/uart.o.d 
	@${RM} ${OBJECTDIR}/Project/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/uart.c  -o ${OBJECTDIR}/Project/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/uart.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/cbuf.o: Project/cbuf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/cbuf.o.d 
	@${RM} ${OBJECTDIR}/Project/cbuf.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/cbuf.c  -o ${OBJECTDIR}/Project/cbuf.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/cbuf.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/cbuf.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/can.o: Project/can.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/can.o.d 
	@${RM} ${OBJECTDIR}/Project/can.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/can.c  -o ${OBJECTDIR}/Project/can.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/can.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/can.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/stalk.o: Project/stalk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/stalk.o.d 
	@${RM} ${OBJECTDIR}/Project/stalk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/stalk.c  -o ${OBJECTDIR}/Project/stalk.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/stalk.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/stalk.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_stalk.o: Project/tsk_stalk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_stalk.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_stalk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_stalk.c  -o ${OBJECTDIR}/Project/tsk_stalk.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_stalk.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_stalk.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/flight.o: Project/flight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/flight.o.d 
	@${RM} ${OBJECTDIR}/Project/flight.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/flight.c  -o ${OBJECTDIR}/Project/flight.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/flight.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/flight.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_flightnot.o: Project/tsk_flightnot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_flightnot.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_flightnot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_flightnot.c  -o ${OBJECTDIR}/Project/tsk_flightnot.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_flightnot.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_flightnot.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_flightctrl.o: Project/tsk_flightctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_flightctrl.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_flightctrl.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_flightctrl.c  -o ${OBJECTDIR}/Project/tsk_flightctrl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_flightctrl.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_flightctrl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_idle.o: Project/tsk_idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_idle.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_idle.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_idle.c  -o ${OBJECTDIR}/Project/tsk_idle.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_idle.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_idle.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/Kernel/alarm.o: Kernel/alarm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/alarm.o.d 
	@${RM} ${OBJECTDIR}/Kernel/alarm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/alarm.c  -o ${OBJECTDIR}/Kernel/alarm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/alarm.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/alarm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/pro_man.o: Kernel/pro_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/pro_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/pro_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/pro_man.c  -o ${OBJECTDIR}/Kernel/pro_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/pro_man.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/pro_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/even_man.o: Kernel/even_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/even_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/even_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/even_man.c  -o ${OBJECTDIR}/Kernel/even_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/even_man.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/even_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Kernel/int_man.o: Kernel/int_man.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/int_man.o.d 
	@${RM} ${OBJECTDIR}/Kernel/int_man.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Kernel/int_man.c  -o ${OBJECTDIR}/Kernel/int_man.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Kernel/int_man.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/int_man.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/main.o: Project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/main.o.d 
	@${RM} ${OBJECTDIR}/Project/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/main.c  -o ${OBJECTDIR}/Project/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/main.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/lin.o: Project/lin.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/lin.o.d 
	@${RM} ${OBJECTDIR}/Project/lin.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/lin.c  -o ${OBJECTDIR}/Project/lin.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/lin.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/lin.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/taskdesc.o: Project/taskdesc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/taskdesc.o.d 
	@${RM} ${OBJECTDIR}/Project/taskdesc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/taskdesc.c  -o ${OBJECTDIR}/Project/taskdesc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/taskdesc.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/taskdesc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/timer.o: Project/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/timer.o.d 
	@${RM} ${OBJECTDIR}/Project/timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/timer.c  -o ${OBJECTDIR}/Project/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/timer.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/uart.o: Project/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/uart.o.d 
	@${RM} ${OBJECTDIR}/Project/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/uart.c  -o ${OBJECTDIR}/Project/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/uart.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/cbuf.o: Project/cbuf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/cbuf.o.d 
	@${RM} ${OBJECTDIR}/Project/cbuf.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/cbuf.c  -o ${OBJECTDIR}/Project/cbuf.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/cbuf.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/cbuf.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/can.o: Project/can.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/can.o.d 
	@${RM} ${OBJECTDIR}/Project/can.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/can.c  -o ${OBJECTDIR}/Project/can.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/can.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/can.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/stalk.o: Project/stalk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/stalk.o.d 
	@${RM} ${OBJECTDIR}/Project/stalk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/stalk.c  -o ${OBJECTDIR}/Project/stalk.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/stalk.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/stalk.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_stalk.o: Project/tsk_stalk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_stalk.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_stalk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_stalk.c  -o ${OBJECTDIR}/Project/tsk_stalk.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_stalk.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_stalk.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/flight.o: Project/flight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/flight.o.d 
	@${RM} ${OBJECTDIR}/Project/flight.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/flight.c  -o ${OBJECTDIR}/Project/flight.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/flight.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/flight.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_flightnot.o: Project/tsk_flightnot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_flightnot.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_flightnot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_flightnot.c  -o ${OBJECTDIR}/Project/tsk_flightnot.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_flightnot.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_flightnot.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_flightctrl.o: Project/tsk_flightctrl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_flightctrl.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_flightctrl.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_flightctrl.c  -o ${OBJECTDIR}/Project/tsk_flightctrl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_flightctrl.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_flightctrl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Project/tsk_idle.o: Project/tsk_idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Project" 
	@${RM} ${OBJECTDIR}/Project/tsk_idle.o.d 
	@${RM} ${OBJECTDIR}/Project/tsk_idle.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Project/tsk_idle.c  -o ${OBJECTDIR}/Project/tsk_idle.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Project/tsk_idle.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -O0 -msmart-io=1 -Wall -msfr-warn=off    -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Project/tsk_idle.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Kernel/scheduler.o: Kernel/scheduler.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/scheduler.o.d 
	@${RM} ${OBJECTDIR}/Kernel/scheduler.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  Kernel/scheduler.s  -o ${OBJECTDIR}/Kernel/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -Wa,-MD,"${OBJECTDIR}/Kernel/scheduler.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/scheduler.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/Kernel/scheduler.o: Kernel/scheduler.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Kernel" 
	@${RM} ${OBJECTDIR}/Kernel/scheduler.o.d 
	@${RM} ${OBJECTDIR}/Kernel/scheduler.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  Kernel/scheduler.s  -o ${OBJECTDIR}/Kernel/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -Wa,-MD,"${OBJECTDIR}/Kernel/scheduler.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp=${DFP_DIR}/xc16
	@${FIXDEPS} "${OBJECTDIR}/Kernel/scheduler.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    Linker/p30F4011.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG=__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel"  -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x84F   -Wl,,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D__DEBUG=__DEBUG,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST)  -mdfp=${DFP_DIR}/xc16 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   Linker/p30F4011.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -std=gnu99 -I"../../../../../Program Files (x86)/Microchip/xc16/v1.35/support/dsPIC30F/h" -I"Project" -I"Kernel" -Wl,,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST)  -mdfp=${DFP_DIR}/xc16 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/car-light-controller.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf   -mdfp=${DFP_DIR}/xc16 
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
