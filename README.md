# Car light controller
This small C program for dsPIC30F4011 enables to monitor the state of the stalk switches of a car and to control the state of the front lights accordingly.

This project has been carried out as part of my studies. The goal was in particular to experiment with development on PIC microcontrollers with PICos kernel (based on OSEK/VDX standard), in addition to put the knowledge on CAN and LIN buses acquired during the semester into practice.

The stalk switches and front lights, in addition to other parts of the car, are connected to a same CAN network. As required by the project topic, the program has to monitor the state of the stalk switches by listening to the CAN frames sent by the latter. In case the state of the stalk switches changes, the program must compute the new corresponding state of the front lights and send a LIN message to another program, developed by another student, which is in charge of actually updating the state of the front lights.

## Technologies
* C
* CAN
* LIN
* PICos

## Authors
* ALTENBACH Thomas - @taltenba
