/* 
 * @file
 * @brief Lock-free single-producer single-consummer circular buffer.
 * @author Thomas ALTENBACH
 * 
 * Provides a lock-free circular buffer allowing a single producer and a single
 * consummer to access concurrently the buffer.
 */

#include "cbuf.h"

/**
 * @brief Puts data into the buffer.
 * @param cbuf The buffer.
 * @param data The data.
 * @param count The data size, in bytes.
 * @return 0 on success, -1 if there is not enough remaining space in the buffer
 *         to fully put the data.
 * 
 * If remaining buffer space is not enough to store the specified data the
 * buffer is not altered.
 * 
 * This function can be called concurrently with the {@link CBuf_Poll} and 
 * {@link CBuf_Poll_Many} functions.
 * However note that this function is *not* reentrant and should only be called
 * by one single producer.
 */
int16_t CBuf_Put_Many(CBuf* cbuf, void const* data, size_t count) 
{
    /*
     * Use copy of tail as it must only be altered in the buffer if there is 
     * enough remaining space to fully store the data.
     * 
     * Only this function modifies the tail so reading the tail index is safe 
     * even if a consummer is concurrently accessing the buffer.
     */
    uint8_t tail = cbuf->tail;
    
    size_t i = 0;

    /* 
     * Copy each data byte in the buffer as long as it is not full.
     * The buffer is only allowed to hold CBUF_BUFFER_SIZE - 1 bytes to be able
     * to differentiate the empty and full state of the buffer.  
     * 
     * Reading the head of the buffer here is safe even if it can be modified
     * concurrently by a consummer since byte accesses of byte-aligned memory
     * are atomic.
     */
    while (i < count && (tail + 1) % CBUF_BUFFER_SIZE != cbuf->head)
    {
      /*
       * Copy the byte to the buffer.
       * 
       * The data can be safely accessed concurrently since a concurrent
       * consummer would stop at the tail.
       */
      cbuf->data[tail] = ((uint8_t const*) data)[i++];

      /* Update the local copy of the tail */
      tail = (tail + 1) % CBUF_BUFFER_SIZE;
    }

    /* Check if the data was fully copied */
    if (i != count)
      return -1; /* If not let the buffer unaltered and return -1 */

    /*
     * Compiler memory barrier, preventing that the compiler performs the update
     * of the tail before writing the string to the buffer.
     */
    __asm__ __volatile__ ("" ::: "memory");

    /*
     * Update the buffer tail index.
     * 
     * Since the tail is only written here and the access of byte-aligned memory
     * is atomic this operation can be done without the need of a lock.
     */
    cbuf->tail = tail;

    /* Return 0 for success */
    return 0;
}

/**
 * @brief Removes and returns the byte at the head of the buffer.
 * @param cbuf The buffer.
 * @return The unsigned byte at the head of the buffer if the latter is not
 *         empty, -1 otherwise.
 * 
 * This function can be called concurrently with the {@link CBuf_Put_Many}
 * function.
 * However note that this function is *not* reentrant and should only be called
 * by one single consummer. Moreover this function must *not* be called
 * concurrently with the {@link CBuf_Poll_Many} function.
 */
int16_t CBuf_Poll(CBuf* cbuf)
{
    /* 
     * Check if the buffer is empty.
     * 
     * Only this function modifies the head so reading the head index is safe 
     * even if a producer is concurrently accessing the buffer.
     * 
     * Moreover, reading the tail of the buffer here is safe even if it can be
     * modified concurrently by a producer since byte accesses of byte-aligned
     * memory are atomic.
     */
    if (cbuf->head == cbuf->tail)
        return -1; /* If so, do nothing and return -1 */

    /*
     * If not, get the head byte of the buffer.
     * 
     * The data can be safely accessed concurrently since a concurrent producer
     * would stop before the head.
     */
    int16_t byte = cbuf->data[cbuf->head];

    /*
     * Compiler memory barrier, preventing that the compiler performs the update
     * of the head before reading the byte from the buffer.
     */
    __asm__ __volatile__ ("" ::: "memory");

    /*
     * Increment the head index.
     * 
     * Since the head is only written here and the access of byte-aligned memory
     * is atomic this operation can be done without the need of a lock.
     */
    cbuf->head = (cbuf->head + 1) % CBUF_BUFFER_SIZE;

    /* Return the removed byte */
    return byte;
}

/**
 * @brief Removes and returns multiple bytes at the head of the buffer.
 * @param cbuf The buffer
 * @param data The removed data.
 * @param count The maximum number of bytes to remove.
 * @return The number of bytes removed.
 * 
 * This function can be called concurrently with the {@link CBuf_Put_Many}
 * function.
 * However note that this function is *not* reentrant and should only be called
 * by one single consummer. Moreover this function must *not* be called
 * concurrently with the {@link CBuf_Poll} function.
 */
uint16_t CBuf_Poll_Many(CBuf* cbuf, void* data, size_t count)
{
    /* If the maximum number of bytes is zero */
    if (count == 0)
        return 0; /* Return immediately */

    size_t i = 0;
    int16_t c;

    /* 
     * Remove and copy bytes from the buffer head, as long as the maximum number
     * of bytes has not been reached and the buffer is not empty.
     */
    while (i < count && (c = CBuf_Poll(cbuf)) != -1)
        ((uint8_t*) data)[i++] = c;

    /* Return the number of bytes removed */
    return i;
}
