/* 
 * @file
 * @brief Front light notifier task, notifying of front light state update.
 * @author Thomas ALTENBACH
 */

#include "define.h"
#include "stalk.h"
#include "flight.h"

TASK(TSK_FLIGHT_NOT)
{
    FLight_Init(Stalk_Get_State());
    
    while(1)
    {
        WaitEvent(EVT_STALK_STATE_CHANGED);
        ClearEvent(EVT_STALK_STATE_CHANGED);
        
        FLight_Notify_State_Updated(Stalk_Get_State());
    }
}
