/* 
 * @file
 * @brief UART-related functions.
 * @author Thomas ALTENBACH
 */

#include <xc.h>
#include "system.h"
#include <libpic30.h>
#include "define.h"
#include <string.h>
#include "err.h"
#include "uart.h"
#include "timer.h"

/**
 * If defined, all UART break transmission and reception is ignored.
 * In this case, the UART_Send_Break function is a no-op and the
 * UART_Receive_On_Break function has the same effect as calling the
 * UART_Receive_Async function with the exception that the timeout is not
 * taken into account.
 * 
 * This mode has been created since the hardware used to test this program was
 * not able to send UART break. This mode is therefore intended for testing
 * purposes only
 */
//#define NO_BREAK_MODE

/**
 * Desired Baud Rate
 */
#define BAUDRATE        9600UL

/**
 * U1BRG register value
 */
#define BRGVAL          (FCY / (BAUDRATE * 16) - 1)

/**
 * Pointer to location where to store the next received byte.
 */
static uint8_t* g_rx_cur = NULL;

/**
 * The remaining number of bytes to receive.
 */
static uint16_t g_rx_remaining = 0;

/**
 * If non-zero, indicate the error that occurred during the reception process.
 * If zero, no error occurred.
 */
static uint16_t g_rx_err = 0;

/**
 * Boolean, set if received bytes must be ignored until the reception of an
 * UART break, cleared otherwise.
 */
static int16_t g_rx_waiting_break = 0;

/**
 * The reception timeout.
 */
static uint16_t g_rx_timeout = 0;

/**
 * @brief Initializes the UART.
 * 
 * Set up UART for 9600 bauds transmission, 8-bit data, no parity, and 1 stop
 * bit.
 * 
 * The UART driver uses the T1 timer.
 */
void UART_Init(uint16_t loopback)
{
    /* Initialize timeout timer */
    T3_Init();
    
    /* Clear UART1 registers */
    U1MODE = 0x0000;
    U1STA = 0x0000;

    /* Enable U1ATX and U1ARX instead of U1TX and U1RX pins */
    U1MODEbits.ALTIO = 1;
    /* Loopback mode */
    U1MODEbits.LPBACK = loopback;

    /* Enable UART1 module */
    U1MODEbits.UARTEN = 1;
    /* Load UART1 baud rate generator */
    U1BRG = BRGVAL;

    /* Clear UART1 Tx interrupt flag */
    IFS0bits.U1TXIF = 0;
    /* Clear UART1 Rx interrupt flag */
    IFS0bits.U1RXIF = 0;
    /* Generate interrupt each time Tx buffer has at least one empty word */
    U1STAbits.UTXISEL = 0;
    /* Generate interrupt on each received byte */
    U1STAbits.URXISEL = 0;
    /* Disable UART1 Tx interrupt */
    IEC0bits.U1TXIE = 0;
    /* Disable UART1 Rx interrupt */
    IEC0bits.U1RXIE = 0;
    
    /* Enable UART1 transmitter */
    U1STAbits.UTXEN = 1;
}

/**
 * @brief Starts the reception of a specified number of bytes.
 * @param buffer The reception buffer.
 * @param size The number of bytes to receive.
 * @param timeout The reception timeout, in milliseconds.
 * @param wait_break Boolean, set iff received bytes must be ignored until the
 *                   reception of an UART break. 
 * @return 0 on success, ERR_BUSY if the UART Rx is already in use.
 */
static int16_t Receive_Async(void* buffer, uint16_t size, uint16_t timeout,
                             int16_t wait_break)
{
    /* Ensure the UART Rx is not already in use */
    if (g_rx_remaining != 0)
        return ERR_BUSY; /* If it is, return on error */
    
    /* Set the reception buffer */
    g_rx_cur = buffer;
    /* Set the number of bytes to receive */
    g_rx_remaining = size;
    /* Clear the error flag */
    g_rx_err = 0;
    /* Set the flag indicating if an UART break must be waited */
    g_rx_waiting_break = wait_break;
    /* Set the reception timeout */
    g_rx_timeout = timeout;
    
#ifdef NO_BREAK_MODE
    /* 
     * Return without starting the timeout timer and the interrupt if the
     * number of bytes to receive is zero.
     */
    if (size == 0)
       return 0;
    
    /*
     * Ignore the timeout if NO_BREAK_MODE is defined and the user wanted to
     * wait for a break.
     */
    if (!wait_break)
        T3_Start(timeout);  /* Start the timeout timer */
#else
    /* Set the flag indicating if an UART break must be waited */
    g_rx_waiting_break = wait_break;
    
    /* If the reception starts immediately */
    if (!g_rx_waiting_break)
    {
        /* 
         * Return without starting the timeout timer and the interrupt if the
         * number of bytes to receive is zero.
         */
        if (size == 0)
            return 0;
        
        /* Start the timeout timer */
        T3_Start(timeout);
    }
#endif /* NO_BREAK_MODE */
    
    /*
     * Compiler memory barrier, preventing the compiler from enabling the
     * interrupt before updating the global variables.
     * 
     * This is needed since the global variables have not been declared as
     * volatile (in order to achieve better performances).
     */
    __asm__ __volatile__ ("" ::: "memory");
    
    /* Enable the UART Rx interrupt */
    IEC0bits.U1RXIE = 1;
    
    /* Return 0 on success */
    return 0;
}

/**
 * @brief Starts the reception of a specified number of bytes.
 * @param buffer The reception buffer.
 * @param size The number of bytes to receive.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the UART Rx is already in use.
 * 
 * After calling this function the UART_Check_Rx_Completed should be called
 * to check if the data have been fully received.
 */
int16_t UART_Receive_Async(void* buffer, uint16_t size, uint16_t timeout)
{
    return Receive_Async(buffer, size, timeout, 0);
}

/**
 * @brief Starts the reception of a specified number of bytes when an UART
 *        break is received.
 * @param buffer The reception buffer.
 * @param size The number of bytes to receive.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the UART Rx is already in use.
 * 
 * All bytes before the reception of an UART break will be discarded.
 * 
 * The timeout timer will start when the UART break is received.
 * 
 * After calling this function the UART_Check_Rx_Completed should be called
 * to check if the data have been fully received.
 */
int16_t UART_Receive_On_Break(void* buffer, uint16_t size, uint16_t timeout)
{
    return Receive_Async(buffer, size, timeout, 1);
}

/**
 * @brief Checks if the current data reception has been completed.
 * @return 0 if the data has been successfully received, ERR_IN_PROGRESS if they
 *         have not been fully received, another error code if an error occured
 *         during the reception of the data.
 */
int16_t UART_Check_Rx_Completed()
{
    /* Check if an error occurred during the reception */
    if (g_rx_err != 0) {
        /* If so, clear the number of remaining bytes */
        g_rx_remaining = 0;
        /* And return the error code */
        return g_rx_err;
    }
    
    /* Check if the data has been fully received */
    if (g_rx_remaining == 0)
        return 0; /* If so, return 0 */
    
    /* Check if the timeout timer has elapsed */
    if (T3_Has_Elapsed())
    {
        /* If so, clear the number of remaining bytes */
        g_rx_remaining = 0;
        /* And indicate that the timeout has elapsed */
        return ERR_TIMEOUT;
    }
    
    /* No error occurred but the data has been fully received yet */
    return ERR_IN_PROGRESS;
}

/**
 * @brief Transmits a byte through UART.
 * @param ch The byte.
 * 
 * This function waits until the byte can be written in the transmission buffer.
 */
void UART_Put_Char(uint8_t ch)
{
    /* Wait until there is space in the transmission buffer */
    while (U1STAbits.UTXBF);
    
    /* Copy the character to the transmission buffer */
    U1TXREG = ch;
}

/**
 * @brief Transmits an UART break.
 * 
 * The UART break is transmitted synchronously, meaning this function returns
 * only when the UART break has been transmitted.
 */
void UART_Send_Break()
{
#ifndef NO_BREAK_MODE
    /* Wait transmitter to be idle */
    while (!U1STAbits.TRMT);
    
    /* Start the transmission of break bits */
    U1STAbits.UTXBRK = 1;
    
    /* Wait for at least 13 baud clocks */
    __delay_us(13 * 1000000UL / BAUDRATE + 1);
    
    /* Stop the transmission of break bits */
    U1STAbits.UTXBRK = 0;
    
    /* Wait for the stop bit to be written */
    __delay_us(1000000UL / BAUDRATE + 1);
#endif
}

/* UART Rx interrupt handler */
void _ISR __attribute__((no_auto_psv)) _U1RXInterrupt(void)
{
    /* Clear the interrupt flag */
    IFS0bits.U1RXIF = 0;
    
    /* While data is still expected and there is available data to read */
    while (g_rx_remaining != 0 && U1STAbits.URXDA) {
        /* Check if a frame error occurred */
        uint16_t ferr = U1STAbits.FERR;
        /* Retrieve the next byte in reception register */
        uint16_t c = U1RXREG;
        
        /* If we are not waiting for an UART break */
        if (!g_rx_waiting_break)
        {
            /* Copy the received byte to the reception buffer */
            *g_rx_cur++ = c;
            /* Update the remaining number of bytes to receive */
            --g_rx_remaining;
        }
        
        /* If a frame error occurred */
        if (ferr == 1)
        {
            /* If the received byte is zero, a break occurred */
            if (c == 0 && g_rx_waiting_break)
            {
                /* In case we were waiting for a break, start the timeout timer */
                T3_Start(g_rx_timeout);
                /* And indicate we are not waiting for a break anymore */
                g_rx_waiting_break = 0;
            }
            else
            {
                /* 
                 * Otherwise the frame error was not expected, indicate the 
                 * error.
                 */
                g_rx_err = ERR_FRAME;
            }
        }
    }
    
    /* Check if an overrun error occurred */
    if (U1STAbits.OERR == 1)
    {
        /* If so, indicate the error */
        g_rx_err = ERR_OVERRUN;
        /* And clear the UART overrun error flag */
        U1STAbits.OERR = 0;
    }
    
    /* If there is no remaining bytes to receive or an error occurred */
    if (g_rx_remaining == 0 || g_rx_err != 0)
    {
        /* Cancel the timeout timer */
        T3_Cancel();
        /* Disable the Rx interrupt */
        IEC0bits.U1RXIE = 0;
        
        EnterISR();
        SetEvent(TSK_FLIGHT_CTRL_ID, EVT_UART_RX_COMPLETED);
        LeaveISR();
    }
}
