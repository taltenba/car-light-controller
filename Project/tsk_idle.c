/* 
 * @file
 * @brief Idle task, making the CPU enter Idle mode.
 * @author Thomas ALTENBACH
 */

#include <xc.h>
#include "define.h"

TASK(TSK_IDLE)
{
    while (1)
    {
        T1CONbits.TSIDL = 1;  /* Stop OS timer in Idle mode */
        U1MODEbits.USIDL = 0; /* Let UART enabled in Idle mode */
        C1CTRLbits.CSIDL = 0; /* Let CAN enabled in Idle mode */
        
        Idle();               /* Enter Idle mode */
    }
}
