/* 
 * @file
 * @brief CAN network constants and utility macros.
 * @author Thomas ALTENBACH
 */

#ifndef CANNET_H
#define	CANNET_H

#define THIS_NODE_ADDR 0x01
#define STALK_NODE_ADDR 0x50
#define FLIGHT_NODE_ADDR 0x51
#define BROADCAST_NODE_ADDR 0x00

#define ID_FCT_POS    0
#define ID_SRC_POS    8
#define ID_DEST_POS   16
#define ID_SLAVE_POS  28

#define ID_FCT_MASK   ((uint32_t) 0xFF << ID_FCT_POS)
#define ID_SRC_MASK   ((uint32_t) 0xFF << ID_SRC_POS)
#define ID_DEST_MASK  ((uint32_t) 0xFF << ID_DEST_POS)
#define ID_SLAVE_MASK ((uint32_t) 1    << ID_SLAVE_POS)

#define SRC(id)  ((uint32_t) ((id) & ID_SRC_MASK) >> ID_SRC_POS)
#define DEST(id) ((uint32_t) ((id) & ID_DEST_MASK) >> ID_DEST_POS)
#define FCT(id)  ((uint32_t) ((id) & ID_FCT_MASK) >> ID_FCT_POS)

#define ID(src, dest, fct) (((uint32_t) 1      << ID_SLAVE_POS)   | \
                            ((uint32_t) (src)  << ID_SRC_POS)     | \
                            ((uint32_t) (dest) << ID_DEST_POS)    | \
                            ((uint32_t) (fct)  << ID_FCT_POS))

#define AN0_ID    0x00
#define AN1_ID    0x01
#define PORT_A_ID 0x10
#define PORT_B_ID 0x11
#define PORT_C_ID 0x12

#endif	/* CANNET_H */

