/* 
 * @file
 * @brief CAN library.
 * @author Thomas ALTENBACH
 */

#include <xc.h>
#include "define.h"
#include "can.h"
#include "system.h"
#include "cbuf.h"

/**
 * Maximum value for Fcan, in Hz.
 */
#define MAX_FCAN (30 * 1000 * 1000UL)

/**
 * The baud rate, in bit/s.
 */
#define BAUD_RATE (250 * 1000UL)

/**
 * Convert a CAN identifier in standard format to the corresponding identifier
 * in extended format.
 */
#define TO_EXT_FMT_ID(id) ((uint32_t) (id) << 18)

/**
 * Reception queue.
 * Circular buffer used to store the received frames.
 */
static CBuf g_rx_queue = {0};

/**
 * @brief Intializes CAN1 module.
 * @param format The format of the received CAN frames. 
 * @param id_filter The frame ID filter.
 * @param id_mask The frame ID mask.
 * 
 * Only received frames corresponding to the specified filter and mask will be
 * handled by the intialized CAN1 module.
 * 
 * The 11 LSBs of parameters id_filter and id_mask correspond to the standard
 * identifier bits and the following 18 bits (bits 28-11) correspond to the
 * extended identifier bits, and the 3 MSBs are not taken into account.
 */
void CAN_Init(CAN_FrameFormat format, uint32_t id_filter1, 
              uint32_t id_filter2, uint32_t id_mask)
{
    /* Set RF0 (C1RX) to input */
    LATFbits.LATF0 = 1;
    /* Set RF1 (C1TX) to output */
    LATFbits.LATF1 = 0;
    
    /* Choose Fcan = Fcy if Fcy > (MAX_FCAN / 4), Fcan = 4*Fcy otherwise */
    uint16_t cancks = FCY > (MAX_FCAN / 4);
    C1CTRLbits.CANCKS = cancks;
    
    /* Request configuration mode */
    C1CTRLbits.REQOP = 0b100;
    
    /* Wait CAN module is in configuration mode */
    while (C1CTRLbits.OPMODE != 0b100);
    
    /* Propagation segment = 7 TQ */
    C1CFG2bits.PRSEG = 0b110;
    /* Phase buffer segment 1 = 6 TQ */
    C1CFG2bits.SEG1PH = 0b101;
    /* Phase buffer segment 2 = Phase buffer segment 1 = 6 TQ */
    C1CFG2bits.SEG2PHTS = 0;
    /* No CAN bus line filter for wake-up */
    C1CFG2bits.WAKFIL = 0;
    /* Sample three times at the sample point */
    C1CFG2bits.SAM = 1;
    /* Synchronized jump width = 2 TQ */
    C1CFG1bits.SJW = 0b01;
    
    /* 
     * Configure the baud rate.
     * 
     * We have: BRP = Fcan/(2*K*BR) - 1, with K being the number of TQ
     * corresponding to one bit, BR the baud rate and Fcan = Fcy if CANCKS is
     * set, Fcan = 4*Fcy otherwise.
     * 
     * Moreover, with the configuration of CAN1 module, we have:
     *                  K = 1 + 7 + 6 + 6 = 20
     * 
     * In case the computed BRP value is negative, the latter is rounded to 0.
     */
    int16_t brp = ((3 * (1 - cancks) + 1) * FCY) / (2 * 20 * BAUD_RATE) - 1;
    C1CFG1bits.BRP = brp >= 0 ? brp : 0;
    
    /* Reset all CAN interrupt flags */
    C1INTF = 0;
    IFS1bits.C1IF = 0;
    /* Enable interrupt when receiving a frame in RX0 */
    C1INTEbits.RX0IE = 1;
    IEC1bits.C1IE = 1;
    
    /* Configure the use of standard or extended identifiers */
    C1RXF0SIDbits.EXIDE = format;
    C1RXF1SIDbits.EXIDE = format;
    /* Match only if message type correspond to EXIDE configuration */
    C1RXM0SIDbits.MIDE = 1;
    
    if (format == CAN_FORMAT_STD)
    {
        id_mask = TO_EXT_FMT_ID(id_mask);
        id_filter1 = TO_EXT_FMT_ID(id_filter1);
        id_filter2 = TO_EXT_FMT_ID(id_filter2);
    }
    
    /*
     * Configure RX0 acceptance filters.
     * 
     * In big endian, filter bits are: xxxS SSSS SSSS SSEE EEEE EEEE EEEE EEEE
     * With 'E' corresponding to extended format bits, 'S' to standard format
     * bits and 'x' to unused bits.
     */
    uint16_t sid = (id_filter1 & 0x1FFC0000) >> 18;
    uint16_t eidl = (id_filter1 & 0x0000003F) << 10;
    uint16_t eidh = (id_filter1 & 0x0003FFC0) >> 6;
    
    C1RXF0SIDbits.SID = sid;
    C1RXF0EIDL = eidl;
    C1RXF0EIDH = eidh;
    
    sid = (id_filter2 & 0x1FFC0000) >> 18;
    eidl = (id_filter2 & 0x0000003F) << 10;
    eidh = (id_filter2 & 0x0003FFC0) >> 6;
    
    C1RXF1SIDbits.SID = sid;
    C1RXF1EIDL = eidl;
    C1RXF1EIDH = eidh;
    
    /* Configure RX0 acceptance filter mask, same principle as for filters */
    C1RXM0SIDbits.SID = (id_mask & 0x1FFC0000) >> 18;
    C1RXM0EIDL = (id_mask & 0x0000003F) << 10;
    C1RXM0EIDH = (id_mask & 0x0003FFC0) >> 6;
    
    /* Request normal mode */
    C1CTRLbits.REQOP = 0b000;
    
    /* Wait CAN module is in normal mode */
    while (C1CTRLbits.OPMODE != 0b000);
}

/**
 * @brief Tries to send the specified frame through the CAN1 module.
 * @param frame The CAN frame to send.
 * @return 1 if the frame was successfully sent, 0 otherwise.
 * 
 * This function checks if a CAN1 transmission buffer is free and if at least
 * one is available the frame is copied in the latter and 0 is returned, 
 * otherwise if all buffers are currently full, nothing is done and -1 is 
 * returned.
 */
int16_t CAN_Send(CAN_Frame const* frame)
{
    /* Pointer to the SID register of the Tx buffer to use */
    uint16_t volatile* tx_buf;
            
    /* Use the first available transmit buffer */
    if (C1TX0CONbits.TXREQ == 0)
        tx_buf = &C1TX0SID;
    else if (C1TX1CONbits.TXREQ == 0)
        tx_buf = &C1TX1SID;
    else if (C1TX2CONbits.TXREQ == 0)
        tx_buf = &C1TX2SID;
    else
        return -1; /* No transmit buffer available, do nothing and return -1 */
    
    uint32_t ext_fmt_id;
    
    if (frame->format == CAN_FORMAT_STD)
        ext_fmt_id = TO_EXT_FMT_ID(frame->id);
    else
        ext_fmt_id = frame->id;
    
    /* SID register value */
    uint16_t sid = 0;
    /* Write 11 bits of standard identifier */
    sid |= ((ext_fmt_id & 0x1F000000) >> 13) | ((ext_fmt_id & 0x00FC0000) >> 16);
    /* Set SRR iff this is an ext. format frame or a std. format remote frame */
    sid |= (frame->type | frame->format) << 1;
    /* Set TXIDE iff this is an extended format frame */
    sid |= frame->format;
    
    /* 
     * EID register value.
     * 
     * Write 12 MSBs of extended identifier (this has no effect in case the
     * frame is a standard format frame).
     */
    uint16_t eid = ((ext_fmt_id & 0x0003C000) >> 2) |
                   ((ext_fmt_id & 0x00003FC0) >> 6);
    
    /* DLC register value */
    uint16_t dlc = 0;
    /* Write 6 LSBs of extended identifier */
    dlc |= (ext_fmt_id & 0x0000003F) << 10;
    /* Set RTR iff this is a remote frame */
    dlc |= frame->type << 9;
    /* Set DLC to the number of bytes of data */
    dlc |= frame->data_len << 3;
    
    /*
     * For any Tx buffer, registers are contiguous and in this order in memory,
     * starting at the lowest address: SID, EID, DLC, B1, B2, B3, B4, CON
     */
    
    /* Set the value of SID, EID and DLC registers */
    tx_buf[0] = sid;
    tx_buf[1] = eid;
    tx_buf[2] = dlc;
    
    /*
     * Set the data to transmit (always copy the maximum number of bytes since
     * its quite free and enable compiler optimizations).
     * 
     * The definition of the CAN_Frame structure ensure that the data member
     * is aligned on a 16-bit boundary and and can therefore safely be accessed
     * word by word to improve efficiency.
     */
    for (int i = 0; i < 4; ++i)
        tx_buf[3 + i] = ((uint16_t*) frame->data)[i];
    
    /*
     * Request transmission by setting TXREQ in CON register.
     * No need for compiler memory barrier since tx_buf is declared volatile.
     */
    tx_buf[7] |= 0x0008;
    
    /* The frame has successfully been written in the transmission buffer */
    return 0;
}

/**
 * @brief Returns the next received CAN frame.
 * @param frame The received frame.
 * @return 0 on success, -1 if the Rx queue is empty.
 */
int16_t CAN_Poll_Rx(CAN_Frame* frame)
{
    /*
     * Retrieve the next frame in the Rx queue.
     * 
     * There is not need to disable temporarly the CAN interrupt since the
     * buffer can be read and written concurrently (see CBuf documentation).
     */
    uint16_t ret = CBuf_Poll_Many(&g_rx_queue, frame, sizeof(CAN_Frame));
    
    /* If the Rx queue is empty */
    if (ret != sizeof(CAN_Frame))
        return -1; /* Return -1 */
    
    /* Otherwise return 0 on success */
    return 0;
}

/**
 * @brief Reads a CAN frame received in RX0.
 * @param frame The read frame.
 */
static void Read_Frame(CAN_Frame* frame)
{
    /* Read the frame format */
    frame->format = C1RX0SIDbits.RXIDE;

    if (frame->format == CAN_FORMAT_EXT)
    {
        /* Extended frame format */
        
        /* Read the standard and extended frame identifier */
        frame->id = 0;
        frame->id |= (uint32_t) C1RX0SIDbits.SID << 18;
        frame->id |= (uint32_t) (C1RX0EID & 0x0FFF) << 6;
        frame->id |= (uint32_t) (C1RX0DLC & 0xFC00) >> 10;

        /* And read the RXRTR */
        frame->type = C1RX0DLCbits.RXRTR;
    }
    else
    {
        /* Standard frame format */
        
        /* Read the standard frame identifier */
        frame->id = C1RX0SIDbits.SID;
        /* Read the SRR */
        frame->type = C1RX0SIDbits.SRR;
    }

    /* Read the data length */
    frame->data_len = C1RX0DLCbits.DLC;
    
    /*
     * Pointer to the start of the data of the received frame.
     * B1, B2, B3 and B4 registers are contiguous in memory.
     */
    uint16_t volatile* src_data = &C1RX0B1;
    
    /*
     * The definition of the CAN_Frame structure ensure that the data member
     * is aligned on a 16-bit boundary and and can therefore safely be accessed
     * word by word to improve efficiency.
     */
    uint16_t* dest_data = (uint16_t*) frame->data;
        
    /*
     * Read the data word by word (always copy the maximum number of bytes since
     * it is quite free and enables compiler optimizations).
     */
    for (int i = 0; i < 4; ++i)
        *dest_data++ = *src_data++;

    /* 
     * Clear RX0 RXFUL flag.
     * No need of compiler memory barrier since C1RX0CONbits and src_data are
     * declared volatile.
     */
    C1RX0CONbits.RXFUL = 0;
}

/* C1 CAN module interrupt handler */
void _ISR __attribute__((no_auto_psv)) _C1Interrupt()
{
    /* Clear CAN1 interrupt flag */
    IFS1bits.C1IF = 0;
    
    /* If a frame has been received */
    if (C1INTFbits.RX0IF)
    {
        CAN_Frame frame;
        /* Read the frame */
        Read_Frame(&frame);
        /* Put the frame in the buffer. */
        CBuf_Put_Many(&g_rx_queue, &frame, sizeof(CAN_Frame));
        
        /* Clear RX0 interrupt flag */
        C1INTFbits.RX0IF = 0;
        
        EnterISR();
        SetEvent(TSK_STALK_ID, EVT_CAN_FRAME_RECEIVED);
        LeaveISR();
    }
}
