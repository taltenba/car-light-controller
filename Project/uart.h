/* 
 * @file
 * @brief UART-related functions.
 * @author Thomas ALTENBACH
 */

#ifndef UART_H
#define	UART_H

#include <stdint.h>

/**
 * @brief Initializes the UART.
 * 
 * Set up UART for 9600 bauds transmission, 8-bit data, no parity, and 1 stop
 * bit.
 * 
 * The UART driver uses the T1 timer.
 */
void UART_Init(uint16_t loopback);

/**
 * @brief Starts the reception of a specified number of bytes.
 * @param buffer The reception buffer.
 * @param size The number of bytes to receive.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the UART Rx is already in use.
 * 
 * After calling this function the UART_Check_Rx_Completed should be called
 * to check if the data have been fully received.
 */
int16_t UART_Receive_Async(void* buffer, uint16_t size, uint16_t timeout);

/**
 * @brief Starts the reception of a specified number of bytes when an UART
 *        break is received.
 * @param buffer The reception buffer.
 * @param size The number of bytes to receive.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the UART Rx is already in use.
 * 
 * All bytes before the reception of an UART break will be discarded.
 * 
 * The timeout timer will start when the UART break is received.
 * 
 * After calling this function the UART_Check_Rx_Completed should be called
 * to check if the data have been fully received.
 */
int16_t UART_Receive_On_Break(void* buffer, uint16_t size, uint16_t timeout);

/**
 * @brief Checks if the current data reception has been completed.
 * @return 0 if the data has been successfully received, ERR_IN_PROGRESS if they
 *         have not been fully received, another error code if an error occured
 *         during the reception of the data.
 */
int16_t UART_Check_Rx_Completed();

/**
 * @brief Transmits a byte through UART.
 * @param ch The byte.
 * 
 * This function waits until the byte can be written in the transmission buffer.
 */
void UART_Put_Char(uint8_t ch);

/**
 * @brief Transmits an UART break.
 * 
 * The UART break is transmitted synchronously, meaning this function returns
 * only when the UART break has been transmitted.
 */
void UART_Send_Break();

#endif	/* UART_H */
