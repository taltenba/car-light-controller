/* 
 * @file
 * @brief One-shot timer.
 * @author Thomas ALTENBACH
 */

#ifndef TIMER_H
#define	TIMER_H

/**
 * @brief Initializes the T3 timer.
 */
void T3_Init();

/**
 * @brief Starts the timer for running during a given number of milliseconds.
 * @param ms The number of milliseconds (max value is
 *           (65535 * (1000 * 256 / FCY) - 1) milliseconds)
 * @return 0 on successs, ERR_BUSY if the timer is already in use.
 */
int16_t T3_Start(uint16_t ms);

/**
 * @brief Cancel the timer.
 * 
 * If the timer is currently running the timer is stopped, if the timer has
 * already elapsed this function has not effect.
 */
void T3_Cancel();

/**
 * @brief Indicates if the timer has elapsed.
 * @return 1 if the timer has elapsed, 0 otherwise.
 */
int16_t T3_Has_Elapsed();

#endif	/* TIMER_H */

