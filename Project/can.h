/* 
 * @file
 * @brief CAN library.
 * @author Thomas ALTENBACH
 */

#ifndef CAN_H
#define CAN_H

#include <stdint.h>

/**
 * Maximum number of bytes than a CAN frame can contain.
 */
#define MAX_CAN_DATA_LEN 8

/**
 * @enum CAN_FrameType
 * @brief Enumeration of the different frame types.
 */
typedef enum {
    CAN_DATA_FRAME = 0,     /**< Data frame */
    CAN_REMOTE_FRAME = 1    /**< Remote frame */
} CAN_FrameType;

/**
 * @enum CAN_FrameFormat
 * @brief Enumeration of the different frame formats.
 */
typedef enum {
    CAN_FORMAT_STD = 0,     /**< Standard frame format */
    CAN_FORMAT_EXT = 1      /**< Extended frame format */
} CAN_FrameFormat;

/**
 * @struct CAN_Frame
 * @brief Represents a data or remote CAN frame.
 * 
 * The frame can be either in standard or extended format.
 * The provided definition ensure the data member of this structure is aligned
 * on a 16-bit boundary and can therefore be accessed word by word.
 * 
 * If the frame is in stardard format, the 11 LSBs of id struct member
 * correspond to the stardard identifier bits. The other bits are unused.
 * 
 * If the frame is in extended format, the 18 LSBs of the id struct member
 * correspond to the extended identifier bits, the 11 following bits (bits 28-18)
 * correspond to the standard identifier bits, and the 3 MSBs are unused.
 */
typedef struct {
    uint32_t id;                     /**< Identifier of the frame */
    uint8_t type;                    /**< Frame type (@see CAN_FrameType) */
    uint8_t format;                  /**< Frame format (@see CAN_FrameFormat) */
    uint8_t data_len;                /**< Number of bytes of data */
     
    /* Align on a 16-bit boundary to be able to access the data word by word */
    __attribute__ ((aligned (2)))
    uint8_t data[MAX_CAN_DATA_LEN];  /**< Frame data */
} CAN_Frame;

/**
 * @brief Intializes CAN1 module.
 * @param format The format of the received CAN frames. 
 * @param id_filter The frame ID filter.
 * @param id_mask The frame ID mask.
 * 
 * Only received frames corresponding to the specified filter and mask will be
 * handled by the intialized CAN1 module.
 * 
 * If the standard frame is specified, the 11 LSBs of id_filter and id_mask
 * parameters correspond to the stardard identifier bits. The other bits are
 * unused.
 * 
 * If the extended frame format is specified, the 18 LSBs of the id_filter and
 * id_mask parameters correspond to the extended identifier bits, the 11
 * following bits (bits 28-18) correspond to the standard identifier bits, and
 * the 3 MSBs are unused.
 */
void CAN_Init(CAN_FrameFormat format, uint32_t id_filter1, 
              uint32_t id_filter2, uint32_t id_mask);

/**
 * @brief Tries to send the specified frame through the CAN1 module.
 * @param frame The CAN frame to send.
 * @return 1 if the frame was successfully sent, 0 otherwise.
 * 
 * This function checks if a CAN1 transmission buffer is free and if at least
 * one is available the frame is copied in the latter and 0 is returned, 
 * otherwise if all buffers are currently full, nothing is done and -1 is 
 * returned.
 */
int16_t CAN_Send(CAN_Frame const* frame);

/**
 * @brief Returns the next received CAN frame.
 * @param frame The received frame.
 * @return 0 on success, -1 if the Rx queue is empty.
 */
int16_t CAN_Poll_Rx(CAN_Frame* frame);

#endif /* CAN_H */
