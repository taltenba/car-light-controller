/* 
 * @file
 * @brief Front light related functions.
 * @author Thomas ALTENBACH
 */

#ifndef FLIGHTCMD_H
#define	FLIGHTCMD_H

#include "stalk.h"

#define FLIGHT_CONTROLLER 1

void FLight_Init(Stalk_State init_state);

void FLight_Notify_State_Updated(Stalk_State stalk_state);

#if FLIGHT_CONTROLLER == 1
int16_t FLight_Check_State_Update();
#endif /* FLIGHT_CONTROLLER */

#endif	/* FLIGHTCMD_H */

