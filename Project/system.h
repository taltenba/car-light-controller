/* 
 * @file
 * @brief Defines global system constants.
 * @author Thomas ALTENBACH
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

/**
 * Fosc, crystal frequency in Hz.
 */
#define _XTAL_FREQ  (10 * 1000 * 1000UL)

/**
 * On-chip PLL setting.
 */
#define PLLMODE     8

/**
 * Fcy, instruction cycle frequency in Hz.
 */
#define FCY         (_XTAL_FREQ * PLLMODE / 4)

#endif	/* SYSTEM_H */

