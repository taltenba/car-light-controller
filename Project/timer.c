/* 
 * @file
 * @brief One-shot timer.
 * @author Thomas ALTENBACH
 */

#include <xc.h>
#include "system.h"
#include "err.h"

/**
 * Timer prescaler value.
 */
#define PSC 256

/**
 * Number of timer tick per second.
 */
#define TICK_PER_SECOND (FCY / PSC)

/**
 * @brief Initializes the T3 timer.
 */
void T3_Init()
{   
    /* Clear timer registers */
    T3CON = 0x0000;
    
    /* 1:256 prescaler => tick each 12,8 �s with Fcy = 20 MHz */
    T3CONbits.TCKPS = 0b11;
    
    /* Ensure timer interrupt is disable */
    IEC0bits.T3IE = 0;
}

/**
 * @brief Starts the timer for running during a given number of milliseconds.
 * @param ms The number of milliseconds (max value is
 *           (65535 * (1000 * 256 / FCY) - 1) milliseconds)
 * @return 0 on successs, ERR_BUSY if the timer is already in use.
 */
int16_t T3_Start(uint16_t ms)
{
    /* Ensure the timer is not in use */
    if (T3CONbits.TON == 1)
        return ERR_BUSY; /* If it is, return on error */
    
    /* Clear the timer value */
    TMR3 = 0x000;
    
    /* Set the period register */
    PR3 = 1000UL * ms / (1000 * 1000UL / (TICK_PER_SECOND)) + 1;
    
    /* Clear the timer interrupt flag */
    IFS0bits.T3IF = 0;
    /* Enable the timer interrupt */
    IEC0bits.T3IE = 1;
    
    /* Enable the timer */
    T3CONbits.TON = 1;
    
    /* Return on success */
    return 0;
}

/**
 * @brief Cancel the timer.
 * 
 * If the timer is currently running the timer is stopped, if the timer has
 * already elapsed this function has not effect.
 */
void T3_Cancel()
{
    /* Disable the timer */
    T3CONbits.TON = 0;
    /* Disable the timer interrupt */
    IEC0bits.T3IE = 0;
}

/**
 * @brief Indicates if the timer has elapsed.
 * @return 1 if the timer has elapsed, 0 otherwise.
 */
int16_t T3_Has_Elapsed()
{
    /* Check if the interrupt flag is set */
    return IFS0bits.T3IF == 1;
}

/* T1 interrupt handler */
void _ISR __attribute__((no_auto_psv)) _T3Interrupt()
{
    /* Disable the timer */
    T3CONbits.TON = 0;
    /* Disable the timer interrupt */
    IEC0bits.T3IE = 0;
    
    /*
     * The timer interrupt flag is deliberately not cleared since it is used
     * by the T3_Has_Elapsed function.
     */
}
