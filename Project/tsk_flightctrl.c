/* 
 * @file
 * @brief Front light controller task, commanding the front lights.
 * @author Thomas ALTENBACH
 */

#include "define.h"
#include "stalk.h"
#include "flight.h"

#if FLIGHT_CONTROLLER == 1
TASK(TSK_FLIGHT_CTRL)
{
    while(1)
    {
        while (FLight_Check_State_Update() == 0);
        
        WaitEvent(EVT_UART_RX_COMPLETED);
        ClearEvent(EVT_UART_RX_COMPLETED);
    }
}
#endif
