/* 
 * @file
 * @brief LIN-related functions.
 * @author Thomas ALTENBACH
 */

#include <stddef.h>
#include "lin.h"
#include "err.h"
#include "uart.h"

#define HEADER_TIMEOUT_MS 16

/**
 * @brief Send a break followed by a sync byte through the UART.
 */
static void Sync_Break();

/**
 * @brief Compute the checksum of given data.
 * @param data The data.
 * @param length The number of data bytes.
 * @return The computed checksum.
 * 
 * As requested by the LIN specification, an 8-bit one's complement checksum
 * is computed.
 * See https://lniv.fe.uni-lj.si/courses/ndes/LIN_Spec_1_3.pdf for more details.
 */
static uint8_t Checksum(uint8_t const* data, uint8_t length);

/**
 * Pointer to the LIN message being received.
 */
static LIN_Msg* g_rx_msg = NULL;

/**
 * @brief Initializes the LIN driver.
 * 
 * The LIN driver use the USART and the latter must therefore not be used
 * by the user or by another driver.
 */
void LIN_Init(uint16_t loopback)
{
    /* Initialize the UART */
    UART_Init(loopback);
}

/**
 * @brief Send a complete LIN message (header and response).
 * @param msg The message to send.
 * 
 * All fields of the message must have been set, with the exception of the
 * checksum which will be automatically computed by this function. 
 * 
 * This function is intended to be used by the LIN master. 
 */
void LIN_Send_Msg(LIN_Msg const* msg)
{
    /* Send the break and sync */
    Sync_Break();
    /* Send the message ID */
    UART_Put_Char(msg->id);
    /* Send the message data */
    LIN_Send_Msg_Data(msg);
}

/**
 * @brief Send the LIN message response (data and checksum).
 * @param msg The message to send.
 * 
 * Only the length and data field of the LIN message must have been set. The ID
 * field is not used and the checksum will be automatically computed by this
 * function.
 * 
 * This function is intended to be used by a LIN slave.
 */
void LIN_Send_Msg_Data(LIN_Msg const* msg)
{
    /* The msg data */
    uint8_t const* data = msg->data;
    /* The msg length */
    uint8_t len = msg->length;
    
    /* Compute the message checksum */
    uint8_t checksum = Checksum(data, len);
    
    /* Send the message data */
    for (uint8_t i = 0; i < len; ++i)
        UART_Put_Char(*data++);
    
    /* Send the message checksum */
    UART_Put_Char(checksum);
}

/**
 * @brief Sends a LIN message header and start the reception of the response.
 * @param msg The LIN message.
 * @param timeout The timeout for the response reception, in milliseconds.
 * @return 0 on success, or ERR_BUSY if the LIN Rx is already in use.
 * 
 * Only the id and length field of the message must have been set. The length
 * indicates the number of data bytes expected in the message response. The
 * message response data and checksum will be written in the given message
 * instance.
 * 
 * Once this function has been called and the user should call regularly the
 * LIN_Check_Data_Received to check if the message response has been fully
 * received.
 * 
 * This function is intended to be used by the LIN master.
 */
int16_t LIN_Request_Msg(LIN_Msg* msg, uint16_t timeout)
{
    /* Start the reception of the message response */
    int16_t err = LIN_Receive_Msg_Data_Async(msg, timeout);
    
    /* Ensure the UART reception is not already in use */
    if (err != 0)
        return err; /* If it is, return error */
    
    /*
     * Compiler memory barrier, preventing the compiler from sending the message
     * header before starting the reception of the message response.
     */
    __asm__ __volatile__ ("" ::: "memory");
    
    /* Send the break and sync */
    Sync_Break();
    /* Send the message ID */
    UART_Put_Char(msg->id);
    
    /* Return 0 on success */
    return 0;
}

/**
 * @brief Starts the reception of a LIN message response.
 * @param msg The LIN message.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the LIN Rx is already in use.
 * 
 * Only the length field of the LIN message must have been set. The length
 * indicates the number of data bytes expected in the message response. The
 * message response data and checksum will be written in the given message
 * instance.
 * 
 * Once this function has been called and the user should call regularly the
 * LIN_Check_Data_Received to check if the message response has been fully
 * received.
 * 
 * This function is intended to be used by a LIN slave.
 */
int16_t LIN_Receive_Msg_Data_Async(LIN_Msg* msg, uint16_t timeout)
{
    /* Ensure no LIN message is currently being received */
    if (g_rx_msg != NULL)
        return ERR_BUSY; /* If there is one, return error */
    
    /* Set the message instance in which to write the received response */
    g_rx_msg = msg;
    
    /* Start the reception of the message response */
    return UART_Receive_Async(msg->data, msg->length + 1, timeout);
}

/**
 * @brief Checks if a LIN message header has been received.
 * @return The message header ID (>= 0) if a message header has been received,
 *         ERR_IN_PROGRESS otherwise.
 * 
 * At initialization of the LIN driver and after the reception of a message
 * header the listening of message headers is stopped to let the user the
 * possibility to listen for message responses. Therefore to start the listening
 * of the next message header this function must be called at the initialization
 * of the LIN driver and after the processing of each received message header.
 * 
 * This function is intended to be used by a LIN slave.
 */
int16_t LIN_Check_Header_Received()
{
    /* Buffer used to store the sync and ID fields of the LIN message header */
    static uint8_t rx_header[2] = {0};
    
    /* Check if the reception of the header has been completed */
    int16_t err = UART_Check_Rx_Completed();
    
    /* 
     * If a message header has been successfully received and the sync byte is
     * valid.
     */
    if (err == 0 && rx_header[0] == 0x55)
    {
        /* Reset the sync byte value in the buffer */
        rx_header[0] = 0;
        /* Return the received message ID */
        return rx_header[1];
    }
    
    /* 
     * If an error occurred during the reception of the message header or the
     * sync byte of the received header is invalid.
     */
    if (err != ERR_IN_PROGRESS)
    {
        /* 
         * Start the reception of the next message header when a USART break is
         * detected.
         */
        UART_Receive_On_Break(&rx_header, 2, HEADER_TIMEOUT_MS);
    }
    
    /* No message header has been successfully received */
    return ERR_IN_PROGRESS;
}

/**
 * @brief Indicates if a LIN message response has been received.
 * @return 0 if a message response has been successfully received, 
 *         ERR_IN_PROGRESS if the message response has not been fully received,
 *         another error code if an error occured during the reception of the
 *         message header.
 */
int16_t LIN_Check_Data_Received()
{
    /* Check if the reception of message response has been completed */
    int16_t err = UART_Check_Rx_Completed();
    
    /* If the message response has not been successfully received */
    if (err != 0) {
        /* If an error occurred during the reception */
        if (err != ERR_IN_PROGRESS)
        {
            /*
             * Clear the pointer to message being received to let the user
             * receive other LIN messages.
             */
            g_rx_msg = NULL;
        }
        
        /* Return the error code */
        return err;
    }
    
    /*
     * Set the message checksum field to the received checksum.
     * The checksum is the last byte received (in total g_rx_msg->length + 1
     * bytes have been received).
     */
    g_rx_msg->checksum = g_rx_msg->data[g_rx_msg->length];
    
    /* Compute the checksum of the received message response */
    uint8_t checksum = Checksum(g_rx_msg->data, g_rx_msg->length);
    
    /* Compare the computed checksum with the one received */
    if (g_rx_msg->checksum != checksum)
    {
        /* 
         * If they do not match, clear the pointer to message being received to
         * let the user receive other LIN messages.
         */
        g_rx_msg = NULL;
        
        /* And return on error */
        return ERR_CHECKSUM;
    }
    
    /* 
     * Clear the pointer to message being received to let the user receive other
     * LIN messages.
     */
    g_rx_msg = NULL;
    
    /* Return on success */
    return 0;
}

/**
 * @brief Send a break followed by a sync byte through the UART.
 */
static void Sync_Break()
{
    /* Send the UART break */
    UART_Send_Break();
    
    /* Send the sync field */
    UART_Put_Char(0x55);
}

/**
 * @brief Compute the checksum of given data.
 * @param data The data.
 * @param length The number of data bytes.
 * @return The computed checksum.
 * 
 * As requested by the LIN specification, an 8-bit one's complement checksum
 * is computed.
 * See https://lniv.fe.uni-lj.si/courses/ndes/LIN_Spec_1_3.pdf for more details.
 */
static uint8_t Checksum(uint8_t const* data, uint8_t length)
{
    uint16_t sum = 0;
    
    /* 
     * Sum the data byte in a 16-bit unsigned integer, letting the overflows
     * accumulate in the high byte.
     */
    for (uint8_t i = 0; i < length; ++i)
        sum += *data++;
    
    /* 
     * The sum low byte is the result of the data byte sum, without taking the
     * carry into account, and the sum high byte is the sum of the generated
     * carries.
     * 
     * To take the carries into account the low and high byte must therefore be
     * added together, and if a carry is generated during this operation it must
     * be added to the result.
     */
    sum = (sum & 0xFF) + (sum >> 8);
    sum = (sum & 0xFF) + (sum >> 8);

    /* Return the inverse of the computed sum */
    return ~sum;
}
