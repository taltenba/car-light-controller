/* 
 * @file
 * @brief Stalk switch related functions.
 * @author Thomas ALTENBACH
 */

#include <xc.h>
#include "stalk.h"
#include "can.h"
#include "cannet.h"

typedef union {
    struct 
    {
        /* Port A (A0 -> A7) */
        uint8_t : 2;
        uint8_t right_blinker : 1;
        uint8_t no_blinker : 1;
        uint8_t left_blinker : 1;
        uint8_t : 3;
    };
    
    uint8_t raw;
} Port_A;

typedef union {
    struct 
    {
        /* Port B (B0 -> B7) */
        uint8_t rear_washer : 1;
        uint8_t next_song : 1;
        uint8_t : 2;
        uint8_t auto_wiper : 1;
        uint8_t front_washer : 1;
        uint8_t flash : 1;
        uint8_t high_beam : 1;
    };
    
    uint8_t raw;
} Port_B;

typedef union {
    struct 
    {
        /* Port C (C0 -> C7) */
        uint8_t idle_wiper : 1;
        uint8_t intermittent_wiper : 1;
        uint8_t speed_1_wiper : 1;
        uint8_t speed_2_wiper : 1;
        uint8_t rear_wiper : 1;
        uint8_t : 3;
    };
    
    uint8_t raw;
} Port_C;

typedef union 
{
    struct {
        uint16_t : 6;
        uint16_t b0_1 : 2;
        uint16_t b4_7 : 4;
        uint16_t : 4;
    };
    
    Stalk_State state;
} State_Builder;

static uint8_t g_an0 = 0;
static uint8_t g_an1 = 0;
static Port_A g_port_a = {{0}};
static Port_B g_port_b = {{0}};
static Port_C g_port_c = {{0}};

static Stalk_State g_cur_state = {0};

static void Request_State(uint8_t fct_id);
static void Request_Full_State();
static void Update_Fct_State(CAN_Frame const* frame);
static Stalk_State Build_Stalk_State();

void Stalk_Init()
{
    /*
     * Initialize CAN module.
     * 
     * Set filters to receive only frames sent by the stalk switch to the this
     * node (unicast and broadcast).
     */
    CAN_Init(CAN_FORMAT_EXT, 
             ID(STALK_NODE_ADDR, BROADCAST_NODE_ADDR, 0),
             ID(STALK_NODE_ADDR, THIS_NODE_ADDR, 0),
             ID_SLAVE_MASK | ID_DEST_MASK | ID_SRC_MASK);
    
    /* Assume the CAN stalk node has already been initialized */
    Request_Full_State();
    g_cur_state = Build_Stalk_State();
}

Stalk_State Stalk_Update()
{
    // Reset bits corresponding to impulses 
    g_port_b.raw = 0x00;
    g_an0 = 0xFF;
    
    CAN_Frame update;
    update.data_len = 0;
    
    while (CAN_Poll_Rx(&update) == 0 && update.data_len == 1)
        Update_Fct_State(&update);

    if (update.data_len > 1)
    {
        while (CAN_Poll_Rx(&update) == 0 && update.data_len > 1);
        
        Request_Full_State();
    }
     
    g_cur_state = Build_Stalk_State();
    return g_cur_state;
}

Stalk_State Stalk_Get_State()
{
    return g_cur_state;
}

static void Update_Fct_State(CAN_Frame const* frame)
{
    if (frame->data_len != 1)
        return;
    
    switch (FCT(frame->id))
    {
        case AN0_ID:
            g_an0 = frame->data[0];
            break;
        case AN1_ID:
            g_an1 = frame->data[0];
            break;
        case PORT_A_ID:
            g_port_a.raw = frame->data[0];
            break;
        case PORT_B_ID:
            g_port_b.raw = frame->data[0];
            break;
        case PORT_C_ID:
            g_port_c.raw = frame->data[0];
            break;
    }
}

static void Request_State(uint8_t fct_id)
{
    CAN_Frame req;
    req.id = ID(THIS_NODE_ADDR, STALK_NODE_ADDR, fct_id);
    req.type = CAN_REMOTE_FRAME;
    req.format = CAN_FORMAT_EXT;
    req.data_len = 1;
    
    while (CAN_Send(&req) != 0);
    
    CAN_Frame resp;
    
    do
    {
        while (CAN_Poll_Rx(&resp) != 0);
        
        Update_Fct_State(&resp);
    }
    while (DEST(resp.id) == THIS_NODE_ADDR && FCT(resp.id) != fct_id);
}

static void Request_Full_State()
{
    Request_State(AN0_ID);
    Request_State(AN1_ID);
    Request_State(PORT_A_ID);
    Request_State(PORT_C_ID);
}

static Stalk_State Build_Stalk_State()
{
    State_Builder builder;
    Stalk_State* state = &builder.state;
    
    /* 
     * Assume analogic measurements are coded on 8-bits and made with a 5V
     * reference voltage.
     */
    
    if (g_an0 < 0x10)
        state->fog_light_toggle = FOG_FRONT;   /* 0.0V <= AN0 < 0.3V */
    else if (0x34 <= g_an0 && g_an0 < 0x52)
        state->fog_light_toggle = FOG_REAR;    /* 1.0V <= AN0 < 1.6V */
    else
        state->fog_light_toggle = 0;           /* 1.6V <= AN0 <= 5.0V */
    
    if (g_an1 < 0x67)
        state->light_switch = LIGHT_HEAD;      /* 0.0V <= AN1 < 2.0V */
    else if (g_an1 < 0x9A)
        state->light_switch = LIGHT_NONE;      /* 2.0V <= AN1 < 3.0V */
    else if (g_an1 < 0xCD)
        state->light_switch = LIGHT_AUTO;      /* 3.0V <= AN1 < 4.0V */
    else
        state->light_switch = LIGHT_PARKING;   /* 4.0V <= AN1 <= 5.0V */
    
    switch ((g_port_a.raw & 0x1C) >> 2)
    {
        case 0b001:
            state->blinker = BLINKER_RIGHT;
            break;
        case 0b100:
            state->blinker = BLINKER_LEFT;
            break;
        default:
            state->blinker = BLINKER_NONE;
            break;
    }
    
    builder.b0_1 = g_port_b.raw & 0x03;
    builder.b4_7 = (g_port_b.raw & 0xF0) >> 4;
    
    switch (g_port_c.raw & 0x0F)
    {
        case 0b0010:
            state->wiper_speed = SPEED_INTERMITTENT;
            break;
        case 0b0100:
            state->wiper_speed = SPEED_1;
            break;
        case 0b1000:
            state->wiper_speed = SPEED_2;
            break;
        default:
            state->wiper_speed = SPEED_IDLE;
            break;
    }
    
    state->rear_wiper = g_port_c.rear_wiper;
    
    return builder.state;
}
