/* 
 * @file
 * @brief Stalk switch related functions.
 * @author Thomas ALTENBACH
 */

#ifndef STALK_H
#define	STALK_H

#include <stdint.h>

typedef enum
{
    FOG_REAR = 1,
    FOG_FRONT
} Stalk_Fog_Light;

typedef enum
{
    LIGHT_NONE = 0,
    LIGHT_PARKING,
    LIGHT_HEAD,
    LIGHT_AUTO
} Stalk_Lights;

typedef enum
{
    BLINKER_NONE = 0,
    BLINKER_LEFT = 1,
    BLINKER_RIGHT = 2
} Stalk_Blinker;

typedef enum
{
    SPEED_IDLE = 0,
    SPEED_INTERMITTENT,
    SPEED_1,
    SPEED_2
} Stalk_Wiper_Speed;

typedef struct
{
    /* From AN0 */
    uint16_t fog_light_toggle : 2;
    
    /* From AN1 */
    uint16_t light_switch : 2;
    
    /* From port A (A2 -> A4) */
    uint16_t blinker : 2;
    
    /* From port B (B0 -> B7) */
    uint16_t rear_washer : 1;
    uint16_t next_song : 1;
    uint16_t auto_wiper : 1;
    uint16_t front_washer : 1;
    uint16_t flash : 1;
    uint16_t high_beam : 1;
    
    /* From port C (C0 -> C4) */
    uint16_t wiper_speed : 2;
    uint16_t rear_wiper : 1;
    
    uint16_t : 1;
} Stalk_State;

void Stalk_Init();

Stalk_State Stalk_Update();

Stalk_State Stalk_Get_State();

#endif	/* STALK_H */

