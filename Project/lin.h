/** 
 * @file
 * @brief LIN-related functions.
 * @author Thomas ALTENBACH
 */

#ifndef LIN_H
#define	LIN_H

#include <stdint.h>
#include "err.h"

/**
 * Max number of data bytes that can be contained in a LIN message.
 */
#define MAX_DATA_LEN 8

/**
 * @struct LIN_Msg
 * @brief Represents a LIN message.
 * 
 * The structure is packed to ensure that it is possible to write MAX_DATA_LEN+1
 * bytes from the data pointer, the last byte will be stored in the checksum
 * field.
 */
typedef struct __attribute__ ((packed))
{
    uint8_t id;                  /**< The ID of the message */
    uint8_t length;              /**< The number of data bytes in this message */
    uint8_t data[MAX_DATA_LEN];  /**< The message data */
    uint8_t checksum;            /**< The message checksum */
} LIN_Msg;

/**
 * @brief Initializes the LIN driver.
 * 
 * The LIN driver use the USART and the latter must therefore not be used
 * by the user or by another driver.
 */
void LIN_Init(uint16_t loopback);

/**
 * @brief Send a complete LIN message (header and response).
 * @param msg The message to send.
 * 
 * All fields of the message must have been set, with the exception of the
 * checksum which will be automatically computed by this function. 
 * 
 * This function is intended to be used by the LIN master. 
 */
void LIN_Send_Msg(LIN_Msg const* msg);

/**
 * @brief Send the LIN message response (data and checksum).
 * @param msg The message to send.
 * 
 * Only the length and data field of the LIN message must have been set. The ID
 * field is not used and the checksum will be automatically computed by this
 * function.
 * 
 * This function is intended to be used by a LIN slave.
 */
void LIN_Send_Msg_Data(LIN_Msg const* msg);

/**
 * @brief Sends a LIN message header and start the reception of the response.
 * @param msg The LIN message.
 * @param timeout The timeout for the response reception, in milliseconds.
 * @return 0 on success, or ERR_BUSY if the LIN Rx is already in use.
 * 
 * Only the id and length field of the message must have been set. The length
 * indicates the number of data bytes expected in the message response. The
 * message response data and checksum will be written in the given message
 * instance.
 * 
 * Once this function has been called and the user should call regularly the
 * LIN_Check_Data_Received to check if the message response has been fully
 * received.
 * 
 * This function is intended to be used by the LIN master.
 */
int16_t LIN_Request_Msg(LIN_Msg* msg, uint16_t timeout);

/**
 * @brief Starts the reception of a LIN message response.
 * @param msg The LIN message.
 * @param timeout The reception timeout, in milliseconds.
 * @return 0 on success, ERR_BUSY if the LIN Rx is already in use.
 * 
 * Only the length field of the LIN message must have been set. The length
 * indicates the number of data bytes expected in the message response. The
 * message response data and checksum will be written in the given message
 * instance.
 * 
 * Once this function has been called and the user should call regularly the
 * LIN_Check_Data_Received to check if the message response has been fully
 * received.
 * 
 * This function is intended to be used by a LIN slave.
 */
int16_t LIN_Receive_Msg_Data_Async(LIN_Msg* msg, uint16_t timeout);

/**
 * @brief Checks if a LIN message header has been received.
 * @return The message header ID (>= 0) if a message header has been received,
 *         ERR_IN_PROGRESS otherwise.
 * 
 * At initialization of the LIN driver and after the reception of a message
 * header the listening of message headers is stopped to let the user the
 * possibility to listen for message responses. Therefore to start the listening
 * of the next message header this function must be called at the initialization
 * of the LIN driver and after the processing of each received message header.
 * 
 * This function is intended to be used by a LIN slave.
 */
int16_t LIN_Check_Header_Received();

/**
 * @brief Indicates if a LIN message response has been received.
 * @return 0 if a message response has been successfully received, 
 *         ERR_IN_PROGRESS if the message response has not been fully received,
 *         another error code if an error occured during the reception of the
 *         message header.
 */
int16_t LIN_Check_Data_Received();

#endif	/* LIN_H */

