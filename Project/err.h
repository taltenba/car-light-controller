/* 
 * @file
 * @brief Error constants.
 * @author Thomas ALTENBACH
 */

#ifndef ERR_H
#define	ERR_H

enum
{
    ERR_NONE = 0,           /**< No error */
    ERR_EMPTY = -1,         /**< Empty buffer */
    ERR_OVERRUN = -2,       /**< Overrun error */
    ERR_CHECKSUM = -3,      /**< Checksum error */
    ERR_FRAME = -4,         /**< Frame error */
    ERR_IN_PROGRESS = -5,   /**< Operation in progress */
    ERR_BUSY = -6,          /**< Device or resource is busy */
    ERR_TIMEOUT = -7        /**< Timeout error */
};

#endif	/* ERR_H */

