/* 
 * @file
 * @brief Stalk task, listening for stalk state update.
 * @author Thomas ALTENBACH
 */

#include "define.h"
#include "stalk.h"

#define STALK_UPDATE_PERIOD 100

TASK(TSK_STALK) 
{
    //SetRelAlarm(ALARM_STALK_UPDATE, 10, STALK_UPDATE_PERIOD);

    while(1)
    {
        WaitEvent(EVT_CAN_FRAME_RECEIVED);
        ClearEvent(EVT_CAN_FRAME_RECEIVED);
        
        Stalk_Update();
        SetEvent(TSK_FLIGHT_NOT_ID, EVT_STALK_STATE_CHANGED);
    }
}
