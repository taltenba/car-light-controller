/* 
 * @file
 * @brief Front light related functions.
 * @author Thomas ALTENBACH
 */

#include "flight.h"
#include "lin.h"

#define BROADCAST_ONLY_WHEN_CHANGED 0

#define UPDATE_MSG_ID 0x01

typedef enum {
    HEAD_LIGHT_NONE = 0,
    HEAD_LIGHT_DIMMED = 1,
    HEAD_LIGHT_HIGH_BEAM = 2
} Head_Light;

typedef union {
    struct {
        uint8_t blinkers : 2;
        uint8_t parking_lights : 1;
        uint8_t head_lights : 2;
        uint8_t : 3;
    };
    
    uint8_t raw;
} FLight_State;

static FLight_State old_state = {{0}};

static FLight_State Compute_New_State(Stalk_State stalk_state)
{
    FLight_State new_state = {{0}};
    new_state.blinkers = stalk_state.blinker;
    new_state.parking_lights = stalk_state.light_switch != LIGHT_NONE;

    if (stalk_state.light_switch == LIGHT_HEAD)
    {
        if (old_state.head_lights != HEAD_LIGHT_NONE)
            new_state.head_lights = old_state.head_lights;
        else
            new_state.head_lights = HEAD_LIGHT_DIMMED;

        if (stalk_state.high_beam)
            new_state.head_lights = ~new_state.head_lights;
    }
    
    return new_state;
}

static void Broadcast_State(FLight_State state)
{
    LIN_Msg update_msg;
    update_msg.id = UPDATE_MSG_ID;
    update_msg.length = 1;
    update_msg.data[0] = state.raw;
    
    LIN_Send_Msg(&update_msg);
}

void FLight_Init(Stalk_State init_stalk_state)
{
    FLight_State init_state = Compute_New_State(init_stalk_state);
    Broadcast_State(init_state);
    old_state = init_state;
}

void FLight_Notify_State_Updated(Stalk_State stalk_state)
{
    FLight_State new_state = Compute_New_State(stalk_state);
    
#if BROADCAST_ONLY_WHEN_CHANGED == 1
    if (new_state.raw == old_state.raw)
        return;
#endif /* BROADCAST_ONLY_WHEN_CHANGED */
    
    Broadcast_State(new_state);
    old_state = new_state;
}

#if FLIGHT_CONTROLLER == 1

#include "can.h"
#include "cannet.h"

#define UPDATE_RESP_TIMEOUT 1000

typedef union {
    struct {
        uint8_t r_dipped_beam : 1;
        uint8_t : 2;
        uint8_t l_dipped_beam : 1;
        uint8_t : 4;
    };
    
    uint8_t raw;
} Port_A;

typedef union {
    struct {
        uint8_t r_blinker : 1;
        uint8_t : 3;
        uint8_t r_parking_light : 1;
        uint8_t r_high_beam : 1;
        uint8_t : 3;
    };
    
    uint8_t raw;
} Port_B;

typedef union {
    struct {
        uint8_t l_high_beam : 1;
        uint8_t : 2;
        uint8_t l_parking_light : 1;
        uint8_t : 1;
        uint8_t l_blinker : 1;
        uint8_t : 2;
    };
    
    uint8_t raw;
} Port_C;

static void Send_Cmd_Frame(uint16_t fct, uint8_t value)
{
    CAN_Frame cmd_frame;
    cmd_frame.id = ID(THIS_NODE_ADDR, FLIGHT_NODE_ADDR, fct);
    cmd_frame.type = CAN_DATA_FRAME;
    cmd_frame.format = CAN_FORMAT_EXT;
    cmd_frame.data_len = 1;
    cmd_frame.data[0] = value;
    
    CAN_Send(&cmd_frame);
}

int16_t FLight_Check_State_Update()
{
    int16_t id;
    
    if ((id = LIN_Check_Header_Received()) == ERR_IN_PROGRESS)
        return -1;
    
    if (id != UPDATE_MSG_ID)
        return -1;
    
    LIN_Msg update_msg;
    update_msg.length = 1;
    LIN_Receive_Msg_Data_Async(&update_msg, 1000);
    
    int16_t err;
    
    while ((err = LIN_Check_Data_Received()) == ERR_IN_PROGRESS);
    
    if (err != 0)
        return -1;
    
    FLight_State new_state;
    new_state.raw = update_msg.data[0];
    
    Port_A pa;
    pa.r_dipped_beam = new_state.head_lights == HEAD_LIGHT_DIMMED;
    pa.l_dipped_beam = new_state.head_lights == HEAD_LIGHT_DIMMED;
    
    Port_B pb;
    pb.r_blinker = new_state.blinkers == BLINKER_RIGHT;
    pb.r_parking_light = new_state.parking_lights;
    pb.r_high_beam = new_state.head_lights == HEAD_LIGHT_HIGH_BEAM;
    
    Port_C pc;
    pc.l_high_beam = new_state.head_lights == HEAD_LIGHT_HIGH_BEAM;
    pc.l_parking_light = new_state.parking_lights;
    pc.l_blinker = new_state.blinkers == BLINKER_LEFT;
    
    Send_Cmd_Frame(PORT_A_ID, pa.raw);
    Send_Cmd_Frame(PORT_B_ID, pb.raw);
    Send_Cmd_Frame(PORT_C_ID, pc.raw);
    
    return 0;
}

#endif /* FLIGHT_CONTROLLER */
