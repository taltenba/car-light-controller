/* 
 * @file
 * @brief Lock-free single-producer single-consummer circular buffer.
 * @author Thomas ALTENBACH
 * 
 * Provides a lock-free circular buffer allowing a single producer and a single
 * consummer to access concurrently the buffer.
 */

#ifndef CBUF_H
#define	CBUF_H

#include <stdlib.h>
#include <stdint.h>

/**
 * The buffer size.
 */
#define CBUF_BUFFER_SIZE 256

/**
 * @struct CBuf
 * @brief Represents a circular buffer.
 */
typedef struct
{
  uint8_t head;                    /**< Head of the buffer, read position */
  uint8_t tail;                    /**< Tail of the buffer, write position */
  uint8_t data[CBUF_BUFFER_SIZE];  /**< Data of the buffer */
} CBuf;

/**
 * @brief Puts data into the buffer.
 * @param cbuf The buffer.
 * @param data The data.
 * @param count The data size, in bytes.
 * @return 0 on success, -1 if there is not enough remaining space in the buffer
 *         to fully put the data.
 * 
 * If remaining buffer space is not enough to store the specified data the
 * buffer is not altered.
 * 
 * This function can be called concurrently with the {@link CBuf_Poll} and 
 * {@link CBuf_Poll_Many} functions.
 * However note that this function is *not* reentrant and should only be called
 * by one single producer.
 */
int16_t CBuf_Put_Many(CBuf* cbuf, void const* data, size_t count);
        
/**
 * @brief Removes and returns the byte at the head of the buffer.
 * @param cbuf The buffer.
 * @return The unsigned byte at the head of the buffer if the latter is not
 *         empty, -1 otherwise.
 * 
 * This function can be called concurrently with the {@link CBuf_Put_Many}
 * function.
 * However note that this function is *not* reentrant and should only be called
 * by one single consummer. Moreover this function must *not* be called
 * concurrently with the {@link CBuf_Poll_Many} function.
 */
int16_t CBuf_Poll(CBuf* cbuf);

/**
 * @brief Removes and returns multiple bytes at the head of the buffer.
 * @param cbuf The buffer
 * @param data The removed data.
 * @param count The maximum number of bytes to remove.
 * @return The number of bytes removed.
 * 
 * This function can be called concurrently with the {@link CBuf_Put_Many}
 * function.
 * However note that this function is *not* reentrant and should only be called
 * by one single consummer. Moreover this function must *not* be called
 * concurrently with the {@link CBuf_Poll} function.
 */
uint16_t CBuf_Poll_Many(CBuf* cbuf, void* data, size_t count);

#endif	/* CBUF_H */

